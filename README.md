# Typescript Jest starter

# What is this?

This is a simple start for a TypeScript and Jest setup built with Webpack and Babel and NPM.

For best results, use [vsCode](https://code.visualstudio.com/)

# How do I use it?

## Installing

```sh
npm i
```

## Running the app in dev mode

```sh
npm start
```

This will start a webpack dev server with TypeScript type checking and open a browser at http://localhost:9000/

## Running the tests

```sh
npm run test:watch
```

This will start a webpack dev server with TypeScript type checking and open a browser at http://localhost:9000/

## Building the app in prod mode

```sh
npm run build
```

This will transpile the TypeScript files and types into a `dist` folder. 
