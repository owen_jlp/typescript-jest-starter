import {C} from './index'

describe('Initial Test', () => {
  let x: C;
  beforeEach(() => {
    x = new C();
  })
  it('gets the value of X', () => {
    expect(x.getX()).toBe(15)
  })
  it('sets the value of X', () => {
    x.setX(55)
    expect(x.getX()).toBe(55)
  })
})
