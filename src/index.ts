export class C {
    private x = 15;
    getX = () => this.x;
    setX = (newVal: number) => { this.x = newVal; }
}

export let x = new C();

console.log(`This is the value of X: ${x.getX()}`)
