const HtmlWebpackPlugin = require('html-webpack-plugin');
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin');
const path = require('path');

const outputFolder = 'dist'

module.exports = {
    entry: './src/index',
    mode: "production",
    output: {
        path: path.resolve(__dirname, outputFolder),
        filename: 'app.bundle.js'
    },
    resolve: {
        extensions: ['.ts', '.tsx', '.js', '.json']
    },
    module: {
        rules: [{
            // Include ts, tsx, js, and jsx files.
            test: /\.(ts|js)x?$/,
            exclude: /node_modules/,
            loader: 'babel-loader',
        }],
    },
    plugins: [new HtmlWebpackPlugin(), new ForkTsCheckerWebpackPlugin()],
    devServer: {
      contentBase: path.join(__dirname, outputFolder),
      compress: true,
      port: 9000
    }
};
